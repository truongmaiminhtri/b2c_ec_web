﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ec_th2012_c.DAO;
using ec_th2012_c.Models;
using PagedList;
using PagedList.Mvc;

namespace ec_th2012_c.Controllers
{
    public class ProductController : Controller
    {
        int pageSize = 8;
        public ActionResult showCategory(int depID, int? cateID, int? page)
        {
            try
            {
                ViewBag.depID = depID;
                ViewBag.cateID = (cateID ?? -1);
                int pageNumber = (page ?? 1);

                IList<PRODUCTS> ps;

                //return View();
                if (ViewBag.cateID < 0)
                {
                    ViewBag.Title = new DepartmentDAO().getById(depID).NAME;
                    ps = new ProductDAO().getByCategory(depID);
                }
                else
                {
                    ViewBag.Title = new CategoryDAO().getById(cateID.GetValueOrDefault()).NAME;
                    ps = new ProductDAO().getByCategory(depID, cateID.GetValueOrDefault());
                }
                return View(ps.ToPagedList(pageNumber, pageSize));
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }

        public ActionResult showProduct(int proID, int? page)
        {
            try
            {
                PRODUCTS p = new ProductDAO().getById(proID);
                IList<PRODUCTS> ps = new ProductDAO().getSameCategoryItemsById(proID);
                int pageNumber = (page ?? 1);

                ViewBag.SameCategoryItems = ps.ToPagedList(pageNumber, pageSize);
                ViewBag.proID = proID;
                ViewBag.Title = p.NAME;
                ViewBag.categories = new CategoryDAO().getByDepartmentId(p.CATE_DEP.DEPARTMENT_ID);
                new ProductDAO().updateViewNum(proID);
                return View(p);
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpGet]
        public ActionResult Search(string keyword, int? page)
        {
            try
            {
                IList<PRODUCTS> ps;
                @ViewBag.Title = "Search result";
                //if(keyword == null || keyword.Equals(string.Empty))

                ps = new ProductDAO().getByName(keyword);
                int pageNumber = (page ?? 1);

                return View(ps.ToPagedList(pageNumber, pageSize));
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }

        public ActionResult AdvancedSearch(string name, int department, int category, long? MinPrice, long? MaxPrice, int? page)
        {
            try
            {
                @ViewBag.Title = "Search result";
                @ViewBag.name = name;
                @ViewBag.department = department;
                @ViewBag.category = category;
                @ViewBag.MinPrice = (MinPrice ?? 0);
                @ViewBag.MaxPrice = (MaxPrice ?? 0);
                IList<PRODUCTS> ps = new ProductDAO().getByMultiValues(name, department, category, (MinPrice ?? 0), (MaxPrice ?? 0));
                //IList<PRODUCTS> ps = new ProductDAO().testLambdaExpression(Int32.Parse(department));
                int pageNumber = (page ?? 1);

                return View(ps.ToPagedList(pageNumber, pageSize));
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }

        [Authorize(Roles="")]
        [HttpGet]
        public ActionResult Comment()
        {
            try
            {
                COMMENT c = new COMMENT() { PRODUCTID = int.Parse(Request["pID"]), USERID = User.Identity.GetUserId(), MESSAGE = Request["message"], DATACREATE = DateTime.Now };
                new CommentDAO().addComment(c);
                //List<COMMENT> lst = new CommentDAO().getAllSortByTime(c.PRODUCTID).ToList();
                string name = new UserDAO().getAspNetUserbyUserID(c.USERID).UserName;
                //string json = JsonConvert.SerializeObject(c);
                var json = new { UserName = name, DATECREATE = c.DATACREATE.ToString(), MESSAGE = c.MESSAGE};
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
    }
}