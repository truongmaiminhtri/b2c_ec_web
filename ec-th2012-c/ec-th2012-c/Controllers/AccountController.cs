﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using ec_th2012_c.Models;
using ec_th2012_c.DAO;
using System.Net.Mail;

namespace ec_th2012_c.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [Authorize(Roles = "Customer")]
        public ActionResult showOrders()
        {
            var bill = new BillDAO();

            var result = bill.getAllSortByDate(User.Identity.GetUserId());

            if (result == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(result);
        }

        //public ActionResult showOrders1()
        //{
        //    var bill = new BillDAO();

        //    var result = bill.getAllSortByDate(User.Identity.GetUserId());

        //    if (result == null)
        //    {
        //        return RedirectToAction("Error", "Home");
        //    }
        //    return View(result);
        //}
        //public ActionResult showOrder(int BillID)
        //{
        //    var bill = new BillDAO();
        //    var result = bill.getDetailBybillID(BillID);
        //    if (result == null)
        //    {
        //        return RedirectToAction("Error", "Home");
        //    }
        //    return View(result);
        //}



        //public ActionResult showUserInfo(string userID)
        //{
        //    var userDao = new UserDAO();
        //    var result = userDao.getAspNetUserbyUserID(userID);
        //    var billCount = (new BillDAO()).getAllSortByDate(userID);

        //    ViewBag.billCount = billCount.Count;

        //    if (userDao == null)
        //    {
        //        return RedirectToAction("Error", "Home");
        //    }

        //    return View(result);
        //}

        [Authorize(Roles = "Customer")]
        public ActionResult showMyAccount()
        {
            var userDao = new UserDAO();
            var result = userDao.getAspNetUserbyUserID(User.Identity.GetUserId());

            var billCount = (new BillDAO()).getAllSortByDate(User.Identity.GetUserId());

            ViewBag.billCount = billCount.Count;

            if (result == null)
            {
                return RedirectToAction("Error", "Home");
            }

            return View(result);
        }

        //public ActionResult saveUserInfo(string NAME, string ADDRESS, string EMAIL, string PHONE, string BIRTHDAY, string SEX)
        //{
        //    var userDao = new UserDAO();
        //    var result = userDao.getAspNetUserbyUserID(User.Identity.GetUserId());

        //    var billCount = (new BillDAO()).getAllSortByDate(User.Identity.GetUserId());

        //    ViewBag.billCount = billCount.Count;

        //    if (result == null)
        //    {
        //        return RedirectToAction("Error", "Home");
        //    }



        //    return View(result);
        //}
        [Authorize(Roles = "Customer")]
        public ActionResult saveUserInfo(AspNetUsers model)
        {
            EC_DBEntities data = new EC_DBEntities();
            string id = User.Identity.GetUserId();
            var _User = data.AspNetUsers.First(m => m.Id == id);
            _User.NAME = model.NAME;
            _User.ADDRESS = model.ADDRESS;
            _User.EMAIL = model.EMAIL;
            _User.PHONE = model.PHONE;
            if (model.USER_INFO != null)
            {
                _User.USER_INFO.BIRTHDAY = model.USER_INFO.BIRTHDAY;
                _User.USER_INFO.SEX = model.USER_INFO.SEX;
            }
            UpdateModel(_User);
            data.SaveChanges();
            return RedirectToAction("showMyAccount");
        }

        [Authorize(Roles = "Customer")]


        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                   
                    
                    if(user.ACTIVE != "Active")
                    {
                        ModelState.AddModelError("", "Your Account is deactive, check your email for active this");
                        return View(model);
                    }
                    await SignInAsync(user, model.RememberMe);


                    List<CARTS> c = Session["Cart"] as List<CARTS>;
                    string uId = user.Id;
                    if(c != null)
                    {
                        foreach(CARTS _c in c)
                        {
                            _c.ID_USER = uId;
                            new CartDAO().addItem(_c);
                        }
                        Session["Cart"] = null;
                    }
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    ADDRESS = model.address,
                    NAME = model.fullname,
                    EMAIL = model.email,
                    PHONE = model.phone,
                    ACTIVE = "Deactive"
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var currentUser = UserManager.FindByName(user.UserName);

                    var roleResult = UserManager.AddToRole(currentUser.Id, "Customer");

                    USER_INFO _user = new USER_INFO();
                    
                    _user.ID = currentUser.Id;
                    _user.BIRTHDAY = model.birthday;
                    _user.SEX = model.sex;
                    string activeLink = string.Format("{0}://{1}{2}/Account/ActiveAccount?userID={3}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), currentUser.Id);
                    sendAcctiveMail(activeLink, currentUser.EMAIL);

                    var t = DateTime.Now;
                    EC_DBEntities data = new EC_DBEntities();
                    data.USER_INFO.Add(_user);
                    data.SaveChanges();

                    ViewBag.Title =  "Your Account is deactive, check your email for active this";
                    return RedirectToAction("Login", "Account");


                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion


        [AllowAnonymous]
        public ActionResult ForgotPassword(string userName)
        {
            if(userName == null || userName.Equals(string.Empty))
                ViewBag.Title = "Enter your UserName.<br> We will send new password to your Email";
            else
            {
                var user = (new UserDAO()).getAspNetUserbyUserName(userName);
                if (user == null)
                    ViewBag.Title = "userName : '" + userName + "' not Found";
                else
                {
                    if (recovertPassword(userName))
                    {
                        ViewBag.Title = "new password sent to: " + user.EMAIL;
                    }
                    else 
                        ViewBag.Title = "send mail error!!!";
                }
            }


            ViewBag.userName = userName;

            return View();
        }



        private bool sendAcctiveMail(string activeLink, string email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("ecth2012c@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Ecth2012c new Consumer Key";
                mail.Body = @"<h3>Active your account </h3>
                             
                             <strong>Someone just regiter on our Page, but this account is Deactive now</strong>
                             <br>
                             if you are, click this link to Active this account
                             <br>"
                              + activeLink +   
                             "<br> if not, don't worry and do not every think<br><br> Thank you so much!";

                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;

                SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch { return false; }
        }

        [AllowAnonymous]
        public ActionResult ActiveAccount(string userID)
        {
            var user = new UserDAO().getAspNetUserbyUserID(userID);
            if(user == null)
            {
                ViewBag.Mess = "Can not Found this userID";
            }
            else if(user.ACTIVE == "Active")
            {
                ViewBag.Mess = user.UserName + " already active long time";
            }
            else
            {
                EC_DBEntities data = new EC_DBEntities();
                var _User = data.AspNetUsers.First(m => m.Id == userID);
                _User.ACTIVE = "Active";
 
                UpdateModel(_User);
                data.SaveChanges();

                ViewBag.Mess = user.UserName + "  actived";
            }
   

            return View();
        }
        internal bool recovertPassword(string username)
        {


            var user = (new UserDAO()).getAspNetUserbyUserName(username);

            var pass = Guid.NewGuid().ToString("N").Substring(0, 10);

            if (changePassword(user.Id, pass, user.EMAIL))
            {

                return true;

            }
            return false;

        }

        private bool changePassword(string userID, string pass, string email)
        {

            var manager = new UserManager<ApplicationUser>(
                                   new UserStore<ApplicationUser>(
                                   new ApplicationDbContext()));
            manager.RemovePassword(userID);
            IdentityResult result = manager.AddPassword(userID, pass);

            if (result.Succeeded)
            {
                sendPassword(pass, email);
                return true;
            }
            return false;
        }

        private bool sendPassword(string pass, string email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("ecth2012c@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Ecth2012c new password";
                mail.Body = @"<h3>New Password</h3>
                             
                             <strong>This is your new password:</strong>
                             <br>
                             <p>" + pass + @"<p>
                                  <br>
                             <strong>Access our page please!!! and change your password</strong>";

                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;

                SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch { return false; }
        }
    }
}