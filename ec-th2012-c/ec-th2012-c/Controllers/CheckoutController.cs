﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.DAO;
using ec_th2012_c.Models;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace ec_th2012_c.Controllers
{
    public class CheckoutController : Controller
    {
        // GET: Checkout
        public ActionResult Cart()
        {
            try
            {
                List<CARTS> c = new List<CARTS>();
                string uId = this.User.Identity.GetUserId();
                if(uId == null)
                    c = Session["Cart"] as List<CARTS>;
                else
                    c = new CartDAO().getAllItems(uId).ToList();
                if (c != null)
                {
                    foreach (CARTS _c in c)
                    {
                        updateCart(_c);
                    }
                    if (uId == null)
                        c = Session["Cart"] as List<CARTS>;
                    else
                        c = new CartDAO().getAllItems(uId).ToList();
                }
                return View(c);
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
        public ActionResult updateQuantity(CARTS c)
        {
            try 
            {
                int max = new ProductDAO().getById(c.ID_PRODUCT.GetValueOrDefault()).QUANTITY.GetValueOrDefault();
                if (max < 1)
                    return RedirectToAction("RemoveItem", c);
                if (c.QUANTITY > max)
                    c.QUANTITY = max;
                if (User.Identity.GetUserId() != null)
                    new CartDAO().updateItemQuantity(c);
                else
                {
                    List<CARTS> lst = Session["Cart"] as List<CARTS>;
                    int i = lst.FindIndex(x => x.ID_PRODUCT == c.ID_PRODUCT);
                    lst.ElementAt(i).QUANTITY = c.QUANTITY;
                    Session["Cart"] = lst;
                }
                return RedirectToAction("Cart");
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
        public ActionResult addQuantity(CARTS c)
        {
            if(c.QUANTITY < new ProductDAO().getById(c.ID_PRODUCT.GetValueOrDefault()).QUANTITY)
                c.QUANTITY++;
            return RedirectToAction("updateQuantity", c);
        }
        public ActionResult subQuantity(CARTS c)
        {
            if(c.QUANTITY > 1)
                c.QUANTITY --;
            return RedirectToAction("updateQuantity", c);
        }
        public ActionResult RemoveItem(CARTS c)
        {
            try
            {
                removeCart(c);
                return RedirectToAction("Cart");
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
        public ActionResult AddToCart(int pID)
        {
            try
            {
                PRODUCTS p = new ProductDAO().getById(pID);
                List<CARTS> c = new List<CARTS>();
                string uId = this.User.Identity.GetUserId();
                if(uId == null)
                    c = Session["Cart"] as List<CARTS>;
                else
                    c = new CartDAO().getAllItems(uId).ToList<CARTS>();;
                int cart = 0;
                string status = "";
                if (p.QUANTITY < 1)
                    status = "soldout";
                else
                {
                    CARTS _c = new CARTS() { ID_USER = uId, ID_PRODUCT = p.ID, QUANTITY = 1 };
                    if (uId == null)
                    {
                        if (c == null)
                            c = new List<CARTS>();
                        if (!c.Exists(x => x.ID_PRODUCT == p.ID))
                        {
                            _c.PRODUCTS = new ProductDAO().getById(p.ID);
                            c.Add(_c);
                        }
                        else
                            status = "existed";
                        Session["Cart"] = c;
                    }
                    else
                    {
                        if (new CartDAO().addItem(_c))
                        {
                            c.Add(_c);
                        }
                        else
                            status = "existed";
                    }
                }
                /*count items in cart*/
                foreach (CARTS i in c)
                {
                    cart += i.QUANTITY.GetValueOrDefault();
                }
                return Json(new { count = cart, status = status }, JsonRequestBehavior.AllowGet);
                //return Redirect(Request.UrlReferrer.ToString());
                //return RedirectToAction("Cart");
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
        [Authorize(Roles="")]
        public ActionResult Shipping()
        {
            try
            {
                string uId = this.User.Identity.GetUserId();
                foreach(CARTS _c in new CartDAO().getAllItems(uId))
                    updateCart(_c);
                ViewBag.Carts = new CartDAO().getAllItems(uId);
                return View(new UserDAO().getAspNetUserbyUserID(uId));
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Checkout(AspNetUsers user)
        {
            try
            {
                Session["ShippingAddress"] = user.ADDRESS;
                //HttpCookie cookie = new HttpCookie("ShippingAddress", user.ADDRESS);
                //Response.SetCookie(cookie);
                //IList<CARTS> c = new List<CARTS>();
                //string uId = this.User.Identity.GetUserId();
                //c = new CartDAO().getAllItems(uId);
                //return View(c);
                return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }     
        public ActionResult Success()
        {
            if (Request.Params.Get("st") == "Completed")
            {
                try
                {
                    string uId = this.User.Identity.GetUserId();
                    BILL2C bill = new BILL2C();
                    bill.TRANS_ID = Request.Params.Get("tx");
                    bill.DATECREATE = DateTime.Now;
                    bill.TOTAL = Math.Round(Convert.ToDouble(Request.Params.Get("amt")) * 20000, MidpointRounding.AwayFromZero);
                    bill.USERID = uId;
                    bill.STATE = "Dang chuyen";
                    if(Session["ShippingAddress"] != null)
                    {
                        bill.SHIP_ADDRESS = Session["ShippingAddress"] as string;
                        Session["ShippingAddress"] = null;
                    }
                    else
                        return View("Fail");
                    //bill.SHIP_ADDRESS = Request.Cookies["ShippingAddress"].Value;
                    //Response.Cookies.Remove("ShippingAddress");
                    bill.ID = new BillDAO().addBill(bill);
                    if (bill.ID != -1)
                    {

                        List<CARTS> lst = new CartDAO().getAllItems(uId).ToList<CARTS>();
                        bool flag = true;
                        foreach(CARTS c in lst)
                        {
                            BILL2C_DETAIL d = new BILL2C_DETAIL() { ID_PRODUCT = c.ID_PRODUCT.GetValueOrDefault(), ID_BILL2C = bill.ID, QUANTITY = c.QUANTITY, RATE = c.RATE };
                            //d.RATE = c.PRODUCTS.PRICE * (1 - c.PRODUCTS.CATE_DEP.DISCOUNT); //temp

                            if (new ProductDAO().updateQuantity(c.ID_PRODUCT.GetValueOrDefault(), -1 * (c.QUANTITY.GetValueOrDefault())))
                            {
                                new BillDAO().addDetail(d);
                                new CartDAO().removeItem(c.ID);
                            }
                            else
                            {
                                bill.STATE = "Dang kiem tra";
                                flag = false;
                            }
                        }
                        AspNetUsers user = new UserDAO().getAspNetUserbyUserID(uId);
                        if (flag)
                        {
                            sendEmail(user, bill);
                            return View();
                        }
                        else
                        {
                            sendEmail(user);
                            return View("Fail");
                        }
                    }
                    else
                        return View("Fail");
                    
                }
                catch
                {
                    return RedirectToAction("Error", "Home");
                }
            }
            else
                return RedirectToAction("Error", "Home");
        }
        public void sendEmail(AspNetUsers user, BILL2C bill)
        {
            //begin mail
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("ecth2012c@gmail.com");
            mail.To.Add(user.EMAIL);
            mail.Subject = "Ecth2012c INVOICE";
            mail.Body = @"<h3>Receipt for Your Payment to GroupC's Test Store </h3>
                             Transaction ID: " + bill.TRANS_ID + @" | " + bill.DATECREATE + @"
                             <br>
                             Hello " + user.NAME + @",
                             <br>
                                     You have purchased " + bill.TOTAL + @" VND.
                                     <br> You will recive your purchased items as soon as possible.<br> Thank you so much!
                            <br><br> GroupC's Test Store";

            mail.IsBodyHtml = true;
            SmtpServer.Port = 587;

            SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            //end send mail
        }
        public void sendEmail(AspNetUsers user)
        {
            //begin mail
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("ecth2012c@gmail.com");
            mail.To.Add(user.EMAIL);
            mail.Subject = "Ecth2012c TRANSACTION HAS AN ERROR";
            mail.Body = @"<h3>There is an interupt in Your Payment to GroupC's Test Store </h3>
                             Hello " + user.NAME + @",
                             <br>
                                    <br> Your transaction has gone wrong in someway. We will check it and refund to you soon.
                                    <br> Sorry for this inconvenience. We will give you a coupon in next email as a sorry for this problem.
                                    <br> Thank you for using our Store!
                            <br><br> GroupC's Test Store";

            mail.IsBodyHtml = true;
            SmtpServer.Port = 587;

            SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            //end send mail
        }
        
        public void updateCart(CARTS c)
        {
            try
            {
                if(c.PRODUCTS.QUANTITY < 1)
                {
                    removeCart(c);
                    return;
                }
                if (c.QUANTITY > c.PRODUCTS.QUANTITY)
                    c.QUANTITY = c.PRODUCTS.QUANTITY;
                c.RATE = c.PRODUCTS.PRICE * (1 - c.PRODUCTS.CATE_DEP.DISCOUNT) * c.QUANTITY;
                if (User.Identity.GetUserId() != null)
                    new CartDAO().updateItem(c);
                else
                {
                    List<CARTS> lst = Session["Cart"] as List<CARTS>;
                    int i = lst.FindIndex(x => x.ID_PRODUCT == c.ID_PRODUCT);
                    lst.ElementAt(i).QUANTITY = c.QUANTITY;
                    lst.ElementAt(i).RATE = c.RATE;
                    Session["Cart"] = lst;
                }
                return;
            }
            catch
            {
                throw;
            }
        }
        public void removeCart(CARTS c)
        {
            try
            {
                if (User.Identity.GetUserId() != null)
                    new CartDAO().removeItem(c.ID);
                else
                {
                    List<CARTS> lst = Session["Cart"] as List<CARTS>;
                    int i = lst.FindIndex(x => x.ID_PRODUCT == c.ID_PRODUCT);
                    lst.RemoveAt(i);
                    if (lst.Count() == 0)
                        lst = null;
                    Session["Cart"] = lst;
                }
                return;
            }
            catch
            {
                throw;
            }
        }
    }
}