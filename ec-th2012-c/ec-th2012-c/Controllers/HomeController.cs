﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.DAO;
using ec_th2012_c.Models;
using PagedList;
using PagedList.Mvc;
namespace ec_th2012_c.Controllers
{
    public class HomeController : Controller
    {
        int pageSize = 8;
        public ActionResult Index(int? page)
        {
            try
            {
                @ViewBag.Title = "C-Store Online";
                IList<PRODUCTS> ps = new ProductDAO().getAllSortByView();

                int pageNumber = (page ?? 1);

                return View(ps.ToPagedList(pageNumber, pageSize));
            }
            catch
            {
                return Content("Server is in maintainance. Return later!");
            }
        }

       
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }


        public ActionResult Error(int? page)
        {
            try
            {
            @ViewBag.Title = "404";
            IList<PRODUCTS> ps = new ProductDAO().getAllSortByView();
            int pageNumber = (page ?? 1);

            return View(ps.ToPagedList(pageNumber, pageSize));
            }
            catch
            {
                return Content("Server is in maintainance. Return later!");
            }
        }

    }
}