﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ec_th2012_c.Models;

namespace ec_th2012_c.DAO
{
    public class ProductDAO
    {
        private char[] specialChar = { ' ', ',', '.', '?', '<', '>', '/', '\\', '|', '{', '}', '[', ']', ':', ';', '"', '\'', 
                                         '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=' };

        public char[] SpecialChar
        {
            get { return specialChar; }
            set { specialChar = value; }
        }

        public IList<PRODUCTS> getAllSortByView()
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.PRODUCTS
                             where p.ACTIVE == "sale"
                             orderby p.VIEWNUMBER descending
                             select p;
                return result.ToList<PRODUCTS>();
            }
            catch(Exception e)
            {
                throw e;
            }

        }

        public PRODUCTS getById(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                PRODUCTS pro = (from p in data.PRODUCTS
                                where p.ID == id && p.ACTIVE =="sale"
                                select p).Single<PRODUCTS>();
                return pro;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public IList<PRODUCTS> getByName(string name)
        {
            if (name == null || name.Equals(string.Empty))
            {
                return getAllSortByView();
            }
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                name = name.Trim();
                string[] part = name.Split(SpecialChar);
                List<PRODUCTS> result = new List<PRODUCTS>();
                foreach (string p in part)
                {
                    List<PRODUCTS> pro = isContain(data, p);
                    if (pro != null)
                    {
                        foreach (PRODUCTS pr in pro)
                            if (!result.Contains(pr))
                                result.Add(pr);
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<PRODUCTS> isContain(EC_DBEntities data, string s)
        {
            try
            {
                if (s == null || s.Equals(""))
                    return null;
                var result = from p in data.PRODUCTS
                             where p.NAME.ToLower().Contains(s.ToLower()) && p.ACTIVE == "sale"
                             select p;
                if (result == null || result.Count() == 0)
                    return isContain(data, s.Substring(0, s.Length - 1));
                return result.ToList<PRODUCTS>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public IList<PRODUCTS> getByMultiValues(string name, int departID, int categoryID, long minPrice, long maxPrice)
        {
            if (name == null || name.Equals(string.Empty))
            {
                EC_DBEntities data = new EC_DBEntities();
                try
                {
                    var result = from p in data.PRODUCTS
                                 where(departID == -1 || departID == p.CATE_DEP.DEPARTMENT_ID)
                                 && (categoryID == -1 || categoryID == p.CATE_DEP.CATEGORY_ID)
                                 && (minPrice == 0 || ((p.PRICE * (1 - p.CATE_DEP.DISCOUNT)) >= minPrice))
                                 && (maxPrice == 0 || ((p.PRICE * (1 - p.CATE_DEP.DISCOUNT)) <= maxPrice))
                                 && p.ACTIVE == "sale"
                                 select p;

                    return result.ToList<PRODUCTS>();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                name = name.Trim();
                string[] part = name.Split(SpecialChar);
                List<PRODUCTS> result = new List<PRODUCTS>();
                foreach (string p in part)
                {
                    List<PRODUCTS> pro = isContainWithMultiValues(data, p, departID, categoryID,minPrice,maxPrice);
                    //List<PRODUCTS> pro = isContain(data, p);
                    if (pro != null)
                    {
                        foreach (PRODUCTS pr in pro)
                            if (!result.Contains(pr))
                                result.Add(pr);
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<PRODUCTS> isContainWithMultiValues(EC_DBEntities data, string s, int departID, int categoryID, long minPrice, long maxPrice)
        {
            try
            {
                if (s == null || s.Equals(""))
                    return null;
                var result = from p in data.PRODUCTS
                             where p.NAME.ToLower().Contains(s.ToLower())
                             && (departID == -1 || departID == p.CATE_DEP.DEPARTMENT_ID)
                             && (categoryID == -1 || categoryID == p.CATE_DEP.CATEGORY_ID)
                             && (minPrice == 0 || ((p.PRICE * (1 - p.CATE_DEP.DISCOUNT)) >= minPrice))
                             && (maxPrice == 0 || ((p.PRICE * (1 - p.CATE_DEP.DISCOUNT)) <= maxPrice))
                             && p.ACTIVE == "sale"
                             select p;
                if (result == null || result.Count() == 0)
                    return isContain(data, s.Substring(0, s.Length - 1));
                return result.ToList<PRODUCTS>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        public IList<PRODUCTS> getByCategory(int depId)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = data.CATE_DEP.Where(cd => cd.DEPARTMENT_ID == depId).SelectMany(cd => cd.PRODUCTS).Where(p => p.ACTIVE == "sale");
                return result.ToList<PRODUCTS>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        
        public IList<PRODUCTS> getByCategory(int depId, int cateId)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                int cdId = (from cd in data.CATE_DEP
                            where ((cd.CATEGORY_ID == cateId) && (cd.DEPARTMENT_ID == depId))
                            select cd.ID).Single<int>();
                var pro = from p in data.PRODUCTS
                          where p.CATE_DEP_ID == cdId && p.ACTIVE == "sale"
                          select p;
                return pro.ToList<PRODUCTS>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        
        public IList<PRODUCT_INFO> getInfo(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = data.PRODUCTS.Where(p => p.ID == id).SelectMany(p => p.PRODUCT_INFO);
                return result.ToList<PRODUCT_INFO>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        
        public List<PRODUCTS> getSameCategoryItemsById(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();

                PRODUCTS curPro = (from p in data.PRODUCTS
                                where p.ID == id && p.ACTIVE == "sale"
                                select p).Single<PRODUCTS>();
               
                var pro1 = from p in data.PRODUCTS
                          where p.CATE_DEP_ID == curPro.CATE_DEP_ID && p.ACTIVE == "sale" && p.ID != curPro.ID
                          orderby p.VIEWNUMBER descending
                          select p;
                var lst1 = pro1.ToList<PRODUCTS>();

                var pro2 = from p in data.PRODUCTS
                           where p.CATE_DEP.DEPARTMENT_ID == curPro.CATE_DEP.DEPARTMENT_ID && p.ACTIVE == "sale" && p.ID != curPro.ID
                           orderby p.VIEWNUMBER descending
                           select p;

                var lst2 = pro2.ToList<PRODUCTS>();
                if(lst1.Count >= 4)
                {
                    return lst1;
                }
                else
                {
                    return lst1.Union(lst2).ToList<PRODUCTS>();
                }
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<PRODUCTS> testLambdaExpression(int departmentID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.PRODUCTS
                             where p.CATE_DEP.DEPARTMENT_ID == departmentID && p.ACTIVE == "sale"
                             select p;
                return result.ToList<PRODUCTS>();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool updateQuantity(int ID, int quan)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                PRODUCTS old = (from p in data.PRODUCTS
                             where p.ID == ID
                             select p).Single<PRODUCTS>();
                if ((old.QUANTITY + quan) >= 0)
                {
                    old.QUANTITY += quan;
                    data.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void updateViewNum(int ID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                PRODUCTS old = (from p in data.PRODUCTS
                                where p.ID == ID
                                select p).Single<PRODUCTS>();
                old.VIEWNUMBER++;
                data.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

}