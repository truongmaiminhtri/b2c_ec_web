﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ec_th2012_c.Models;

namespace ec_th2012_c.DAO
{
    public class CartDAO
    {
        #region e-cart DAO
        public IList<CARTS> getAllItems(string uId)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.CARTS
                             where p.ID_USER == uId
                             select p;
                return result.ToList<CARTS>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public void updateItemQuantity(CARTS c)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                CARTS old = (from p in data.CARTS
                             where p.ID == c.ID
                             select p).Single<CARTS>();
                old.QUANTITY = c.QUANTITY;
                data.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool addItem(CARTS c)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                List<CARTS> lst = getAllItems(c.ID_USER).ToList<CARTS>();
                if (!lst.Exists(x => x.ID_PRODUCT == c.ID_PRODUCT))
                {
                    c.AspNetUsers = null; c.PRODUCTS = null;
                    data.CARTS.Add(c);
                    data.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void removeItem(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                CARTS old = (from p in data.CARTS
                             where p.ID == id
                             select p).Single<CARTS>();
                data.CARTS.Remove(old);
                data.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void updateItem(CARTS c)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                CARTS old = (from p in data.CARTS
                             where p.ID == c.ID
                             select p).Single<CARTS>();
                old.QUANTITY = c.QUANTITY;
                old.RATE = Math.Round(Convert.ToDouble(c.RATE), MidpointRounding.AwayFromZero);
                data.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}