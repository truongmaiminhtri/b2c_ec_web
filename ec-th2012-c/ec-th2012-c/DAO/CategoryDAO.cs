﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ec_th2012_c.Models;

namespace ec_th2012_c.DAO
{
    public class CategoryDAO
    {
        public IList<CATELOGIES> getAll()
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from c in data.CATELOGIES
                             where c.ACTIVE =="sale" select c;
                return result.ToList<CATELOGIES>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public IList<CATELOGIES> getByDepartmentId(int depId)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var cat = from cd in data.CATE_DEP
                          where cd.DEPARTMENT_ID == depId && cd.ACTIVE == "sale"
                          select cd.CATELOGIES;
                return cat.ToList<CATELOGIES>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public CATELOGIES getById(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                CATELOGIES cat = (from c in data.CATELOGIES
                                  where c.ID == id && c.ACTIVE == "sale"
                                  select c).Single<CATELOGIES>();
                return cat;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public CATELOGIES getByProductId(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                CATELOGIES result = (from p in data.PRODUCTS
                                     where p.ID == id
                                     select p.CATE_DEP.CATELOGIES).Single<CATELOGIES>();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}