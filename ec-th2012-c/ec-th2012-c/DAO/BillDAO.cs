﻿using System;
using System.Collections.Generic;
using System.Linq;
using ec_th2012_c.Models;
using System.Web;

namespace ec_th2012_c.DAO
{
    public class BillDAO
    {
        public IList<BILL2C> getAllSortByDate(string userID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();


                var result = from p in data.BILL2C
                             where p.USERID == userID
                             orderby p.DATECREATE descending
                             select p;
                return result.ToList<BILL2C>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public IList<BILL2C_DETAIL> getDetailBybillID(int billID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.BILL2C_DETAIL
                             where p.ID_BILL2C == billID

                             select p;
                return result.ToList<BILL2C_DETAIL>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public int addBill(BILL2C b)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                List<BILL2C> lst = getAllSortByDate(b.USERID).ToList<BILL2C>();
                if (!lst.Exists(x => x.TRANS_ID == b.TRANS_ID))
                {
                    b.AspNetUsers = null;
                    data.BILL2C.Add(b);
                    data.SaveChanges();
                    return b.ID;
                }
                return -1;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void addDetail(BILL2C_DETAIL c)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                data.BILL2C_DETAIL.Add(c);
                data.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}