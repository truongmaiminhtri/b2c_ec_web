﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ec_th2012_c.Models;

namespace ec_th2012_c.DAO
{
    public class CommentDAO
    {
        public IList<COMMENT> getAllSortByTime(int pID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.COMMENT
                             where p.PRODUCTID == pID
                             orderby p.DATACREATE descending
                             select p;
                return result.ToList<COMMENT>();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void addComment(COMMENT c)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                data.COMMENT.Add(c);
                data.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}