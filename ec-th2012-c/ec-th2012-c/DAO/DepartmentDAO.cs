﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ec_th2012_c.Models;

namespace ec_th2012_c.DAO
{
    public class DepartmentDAO
    {
        public IList<DEPARTMENTS> getAll()
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from d in data.DEPARTMENTS
                             where d.ACTIVE == "sale" select d;
                return result.ToList<DEPARTMENTS>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public DEPARTMENTS getById(int depId)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                DEPARTMENTS dep = (from d in data.DEPARTMENTS
                                   where d.ID == depId && d.ACTIVE == "sale"
                                   select d).Single<DEPARTMENTS>();
                return dep;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public DEPARTMENTS getByProductId(int id)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                DEPARTMENTS result = (from p in data.PRODUCTS
                                      where p.ID == id
                                      select p.CATE_DEP.DEPARTMENTS).Single<DEPARTMENTS>();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}