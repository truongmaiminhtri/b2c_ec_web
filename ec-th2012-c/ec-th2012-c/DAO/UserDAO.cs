﻿using System;
using System.Collections.Generic;
using System.Linq;
using ec_th2012_c.Models;
using System.Web;

namespace ec_th2012_c.DAO
{
    public class UserDAO
    {
        public AspNetUsers getAspNetUserbyUserID(string userID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.AspNetUsers
                             where p.Id == userID
                             select p;
                if (result.Count() == 0)
                    return null;
                else
                    return result.ToList<AspNetUsers>().ElementAt<AspNetUsers>(0);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public AspNetUsers getAspNetUserbyUserName(string userName)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.AspNetUsers
                             where p.UserName == userName
                             select p;
                if (result.Count() == 0)
                    return null;
                else
                    return result.ToList<AspNetUsers>().ElementAt<AspNetUsers>(0);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public USER_INFO getUserInfobyUserID(string userID)
        {
            try
            {
                EC_DBEntities data = new EC_DBEntities();
                var result = from p in data.USER_INFO
                             where p.ID == userID
                             select p;
                if (result.Count() == 0)
                    return null;
                else
                    return result.ToList<USER_INFO>().ElementAt<USER_INFO>(0);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}